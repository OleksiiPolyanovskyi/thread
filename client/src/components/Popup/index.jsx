import React from 'react';
import { Popup } from 'semantic-ui-react';
import PropTypes from 'prop-types';

const PopupComponent = ({ children, content }) => (
  <Popup content={content.map(el => <div>{el}</div>)} trigger={children} />
);

PopupComponent.propTypes = {
  children: PropTypes.objectOf(PropTypes.any).isRequired,
  content: PropTypes.objectOf(PropTypes.any).isRequired
};

export default PopupComponent;
