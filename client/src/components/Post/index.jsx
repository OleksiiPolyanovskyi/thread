import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import AddPost from '../AddPost';
import PopupComponent from '../Popup';

import styles from './styles.module.scss';

const Post = ({
  userId,
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  updatePost,
  uploadImage,
  deletePost
}) => {
  const [editeMode, setMode] = useState(false);
  const {
    id,
    image,
    body,
    user,
    likeCount,
    postReactions,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const changeMode = () => setMode(!editeMode);
  let usersWhoLikedPost = [];
  if (postReactions) {
    usersWhoLikedPost = postReactions
      .filter(react => react.isLike === true)
      .map(react => react.user.username);
  }
  return (
    <Card style={{ width: '100%' }}>
      { editeMode
        ? (
          <AddPost
            editeMode={editeMode}
            post={post}
            updatePost={updatePost}
            changeMode={changeMode}
            addPost={() => {}}
            uploadImage={uploadImage}
          />
        )
        : (
          <>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
              <Card.Meta>
                <span className="date">
                  posted by
                  {' '}
                  {user.username}
                  {' - '}
                  {date}
                </span>
              </Card.Meta>
              <Card.Description>
                {body}
              </Card.Description>
            </Card.Content>
          </>
        )}
      <Card.Content extra>
        <PopupComponent content={usersWhoLikedPost}>
          <Label
            basic
            size="small"
            as="a"
            className={styles.toolbarBtn}
            onClick={() => likePost(id)}
          >
            <Icon name="thumbs up" />
            {likeCount}
          </Label>
        </PopupComponent>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        { userId === post.userId
          ? (
            <>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={changeMode}>
                <Icon name="edit" />
              </Label>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => deletePost(id)}>
                <Icon name="remove" />
              </Label>
            </>
          ) : null }
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  userId: PropTypes.string.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default Post;
