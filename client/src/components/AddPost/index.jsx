import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Icon, Image, Segment, Label } from 'semantic-ui-react';

import styles from './styles.module.scss';

const AddPost = ({
  addPost,
  uploadImage,
  editeMode = false,
  post = null,
  updatePost,
  changeMode
}) => {
  const [body, setBody] = useState(post ? post.body : '');
  const [image, setImage] = useState(undefined);
  const [isUploading, setIsUploading] = useState(false);
  const handleAddPost = async () => {
    if (!body) {
      return;
    }
    if (editeMode) {
      const updated = { ...post };
      updated.body = body;
      updated.imageId = image ? image.imageId : updated.imageId;
      await updatePost(updated);
      changeMode();
    } else {
      await addPost({ imageId: image?.imageId, body });
      setBody('');
      setImage(undefined);
    }
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } finally {
      // TODO: show error
      setIsUploading(false);
    }
  };

  return (
    <Segment className={editeMode ? styles.editeModeFix : null}>
      <Form onSubmit={handleAddPost}>
        <Form.TextArea
          name="body"
          value={body}
          placeholder="What is the news?"
          onChange={ev => setBody(ev.target.value)}
        />
        {image?.imageLink && (
          <div className={styles.imageWrapper}>
            <Image className={styles.image} src={image?.imageLink} alt="post" />
          </div>
        )}
        <div className={styles.buttonsBlock}>
          <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
            <Icon name="image" />
            Attach image
            <input name="image" type="file" onChange={handleUploadFile} hidden />
          </Button>
          {
            !editeMode
              ? <Button floated="right" color="blue" type="submit">Post</Button>
              : (
                <Label basic size="small" as="a" floated="right" className={styles.toolbarBtn} onClick={handleAddPost}>
                  <Icon name="save" />
                </Label>
              )
          }
        </div>
      </Form>
    </Segment>
  );
};

AddPost.propTypes = {
  addPost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  editeMode: PropTypes.bool.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  changeMode: PropTypes.func.isRequired
};

export default AddPost;
