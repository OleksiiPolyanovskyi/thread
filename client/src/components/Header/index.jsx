import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, update }) => {
  const [status, setStatus] = useState(user.status);
  const [edit, setEdit] = useState(false);
  const statusRef = useRef(null);

  const onSave = () => {
    setEdit(false);
    update(user.id, { status });
  };

  const onEdit = () => {
    setEdit(true);
    console.log(statusRef.current);
    statusRef.current.focus();
  };

  const onChange = event => {
    setStatus(event.target.value);
  };
  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <>
              <NavLink exact to="/">
                <HeaderUI>
                  <Image circular src={getUserImgLink(user.image)} />
                  {' '}
                  {user.username}
                </HeaderUI>
              </NavLink>
              <div className={styles.statusField}>
                {!edit
                  ? <Icon className={styles.statusIc} name="edit outline" size="small" onClick={onEdit} />
                  : <Icon className={styles.statusIc} name="save" size="small" onClick={onSave} />}
                <input
                  className={styles.statusText}
                  disabled={!edit}
                  ref={statusRef}
                  value={status}
                  onChange={event => onChange(event)}
                />
              </div>
            </>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
