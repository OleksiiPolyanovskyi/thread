import React, { useState, useRef, useEffect } from 'react';
import usePrevious from 'src/hooks/usePrevious';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input
} from 'semantic-ui-react';
import { updateUserInfo } from './actions';

import styles from './styles.module.scss';

const Profile = ({ user, update }) => {
  const [name, setName] = useState(user.username);
  const [image, setImage] = useState(user.image);
  const [status, setStatus] = useState(user.status);
  const [disable, setDisable] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [error, setError] = useState('');
  const prevUsername = usePrevious(user.username);
  const prevName = usePrevious(name);
  console.log('user', user);
  useEffect(() => {
    if (user.username === prevUsername
      && prevName !== prevUsername) {
      setError('Username is already taken');
    }
    setName(user.username);
  }, [user]);

  const imageInput = useRef(null);

  const handlers = {
    username: setName,
    status: setStatus
  };

  const toggleEditMode = () => {
    setEditMode(!editMode);
    setDisable(!disable);
  };

  const onChangeHandler = (event, field) => {
    handlers[field](event.target.value);
  };
  const onSaveChanges = () => {
    setError('');
    const body = {};
    body.status = status;
    if (name !== user.username) body.username = name;
    update(user.id, body);
    toggleEditMode();
  };
  const handleUploadFile = async ({ target }) => {
    try {
      const { id: imageId, link: imageLink } = await imageService.uploadImage(target.files[0]);
      update(user.id, { imageId });
      setImage({ imageId, imageLink });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image
          centered
          src={getUserImgLink(image)}
          size="medium"
          circular
          onClick={() => imageInput.current.click()}
        />
        <input name="image" type="file" onChange={handleUploadFile} ref={imageInput} hidden />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled={disable}
          value={name}
          onChange={event => onChangeHandler(event, 'username')}
        />
        <br />
        {error ? <span style={{ color: 'red' }}>{error}</span> : null}
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
          onChange={event => onChangeHandler(event, 'email')}
        />
        <br />
        <br />
        <Input
          icon="spy"
          iconPosition="left"
          placeholder="User status"
          type="text"
          disabled={disable}
          value={status}
          onChange={event => onChangeHandler(event, 'status')}
        />
        {editMode ? <div onClick={onSaveChanges} className={styles.textButton}>Save</div>
          : <div onClick={toggleEditMode} className={styles.textButton}>Edit user information</div>}
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  update: PropTypes.func
};

Profile.defaultProps = {
  user: {},
  update: () => {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

export default connect(
  mapStateToProps,
  { update: updateUserInfo }
)(Profile);
