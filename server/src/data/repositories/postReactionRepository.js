import { PostReactionModel, PostModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'post.id'
      ],
      where: { userId, postId },
      include: [{
        model: PostModel,
        attributes: ['id', 'userId']
      }]
    });
  }

  getAllPostsWithReaction(filter) {
    const {
      from: offset,
      count: limit,
      userId
    } = filter;

    return this.model.findAll({
      where: {
        userId,
        isLike: true
      },
      include: [{
        model: PostModel
      }],
      offset,
      limit
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
