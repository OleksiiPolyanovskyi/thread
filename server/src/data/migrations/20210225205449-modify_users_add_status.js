module.exports = {
  up: (queryInterface, Sequelize) => Promise.all(
    [
      queryInterface.addColumn(
        'users',
        'status',
        {
          type: Sequelize.STRING,
          allowNull: true
        }
      )
    ]
  ),

  down: queryInterface => Promise.all(
    [
      queryInterface.removeColumn('users', 'status')
    ]
  )
};
