import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';

async function getPostFromReaction(postId) {
  return postRepository.getPostById(postId);
}

export const getPosts = async filter => {
  if (filter.isLike === 'true') {
    const reactions = await postReactionRepository.getAllPostsWithReaction(filter);
    const postIds = reactions.map(reaction => reaction.dataValues.post.id);
    const posts = await Promise.all(postIds.map(async postId => getPostFromReaction(postId)));
    return posts;
  }
  return postRepository.getPosts(filter);
};

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = async (postId, post) => {
  await postRepository.updateById(postId, post);
  const updated = await postRepository.getPostById(postId);
  return updated;
};

export const deleteById = async postId => {
  const res = await postRepository.deleteById(postId);
  return res;
};

export const getReaction = async (userId, postId) => {
  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  return reaction;
};

export const setReaction = async (userId, { postId, isLike, isDislike }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike && react.isDislike === isDislike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike, isDislike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);
  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike, isDislike });
  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

