import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const updateUserById = async (id, body) => {
  if (body.username) {
    const user = await userRepository.getByUsername(body.username);
    if (user) return getUserById(id);
  }
  return userRepository.updateUserById(id, body);
};
